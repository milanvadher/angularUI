import { Component, OnInit } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // pi chart
  public pieChartLabels: string[] = ['Last-week Attendence', 'This-week Attendence'];
  public pieChartData: number[] = [500, 400];
  public pieChartType = 'pie';

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  // lineChart
  // tslint:disable-next-line:member-ordering
  public lineChartData: Array<any> = [
    [65, 59, 80, 81, 56, 55, 40, 58, 90, 65, 60, 75],
    [28, 48, 40, 19, 86, 27, 90, 25, 65, 79, 52, 90]
  ];
  // tslint:disable-next-line:member-ordering
  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:member-ordering
  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];

  // tslint:disable-next-line:max-line-length
  // tslint:disable-next-line:member-ordering
  public lineChartOptions: Array<any> = ['Jan', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'Octomber', 'November', 'December'];

  // tslint:disable-next-line:member-ordering
  public lineChartType = 'line';

  public randomizeType(): void {
    this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
    this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }


  constructor() { }

  ngOnInit() {
  }

}
